from engine import gamestate
from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sound import *


## Scores
highscore_file = open('highscores.txt','r')
highscore = []


for line in highscore_file:
    line = line.replace('\n','')
    highscore.append(line)
highscore_file.close

def score_screen(background, janela, mouse, click, keyboard):

    global gamestate

    if(keyboard.key_pressed('escape')):
        click.play()
        gamestate[0] = 0
    background.draw()
    for i in range (len(highscore)):
        janela.draw_text(highscore[i],janela.width/3,i*35,size=30,color=(255,255,255))
    janela.update()
