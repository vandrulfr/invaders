from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sound import *
import random
import sys
from engine import gamestate




def menu(janela,mouse,song,click,keyboard,bg0,bg1,bgs,start,options,ebutton,background,score):


    global gamestate

    if not song.is_playing():
        song.play()
    if(mouse.is_button_pressed(1)):
        click.play()
        if mouse.is_over_area([start.x,start.y],[start.x+start.width,start.y+start.height]):
            gamestate[0] = 1
            # log.write('local gamestate:'+str(gamestate))
        elif mouse.is_over_area([options.x,options.y],[options.x+options.width,options.y+options.height]):
            gamestate[0] = 2
        elif mouse.is_over_area([score.x,score.y],[score.x+score.width,score.y+score.height]):
            #highscores = ('highscores.txt','r')
            gamestate[0] = 3
        elif mouse.is_over_area([ebutton.x,ebutton.y],[ebutton.x+ebutton.width,ebutton.y+ebutton.height]):
            quit()


    bg0.x += bgs * janela.delta_time()
    bg0.y += bgs/2 * janela.delta_time()
    bg1.x += bgs * janela.delta_time()
    bg1.y += bgs/2 * janela.delta_time()
    if bg1.x > janela.width:
        bg0.x = 0
        bg1.x = -bg1.width
        bg0.y = random.randint(0,janela.height)
        bg1.y = random.randint(0,janela.height)

    janela.set_background_color = ([0,0,0])


    background.draw()
    bg0.draw()
    bg1.draw()
    start.draw()
    options.draw()
    score.draw()
    ebutton.draw()
    janela.update()
