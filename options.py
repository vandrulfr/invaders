from PPlay.window import *
from PPlay.sound import *
from PPlay.gameimage import *
from settings import *
from engine import gamestate, janela
from render import render


baby = GameImage('resources/img/m_baby.png')
medium = GameImage('resources/img/m_medium.png')
hard = GameImage('resources/img/m_hard.png')
back = GameImage('resources/img/back.png')
baby.set_position(janela.width/2-baby.width/2,janela.height/10)
medium.set_position(janela.width/2-medium.width/2,baby.y+baby.height*1.5)
hard.set_position(janela.width/2-hard.width/2,medium.y+medium.height*1.5)
doom = Sound('resources/sound/doom.ogg')
doom.set_volume(music_volume)
plus = GameImage('resources/img/plus.png')
minus = GameImage('resources/img/minus.png')

def options_menu(keyboard, click, mouse, janela, background, maiden_invaders):

    global gamestate
    global dificuldade
    global music_volume
    global effect_volume

    background.draw()

    # DIFICULDADE #

    if keyboard.key_pressed('escape'):
        click.play()
        gamestate[0] = 0
    if mouse.is_button_pressed(1):
        click.play()

        if mouse.is_over_area([baby.x,baby.y],[baby.x+baby.width,baby.y+baby.height]):
            dificuldade[0] = 1
            doom.stop()
            maiden_invaders.stop()
            maiden_invaders.play()

        if mouse.is_over_area([medium.x,medium.y],[medium.x+medium.width,medium.y+medium.height]):
            dificuldade[0] = 2
            doom.stop()
            maiden_invaders.stop()
            maiden_invaders.play()

        if mouse.is_over_area([hard.x,hard.y],[hard.x+hard.width,hard.y+hard.height]):
            dificuldade[0] = 10
            maiden_invaders.stop()
            doom.stop()
            doom.play()
    # VOLUME #

    janela.draw_text(('Musica: '+str(music_volume)), janela.width/8, janela.height-janela.height/10,size=12,color=(255,255,255))
    musicaup = plus
    musicaup.set_position(janela.width/8-plus.width/2,janela.height-janela.height/12)

    janela.draw_text(('SFX: '+str(effect_volume)), janela.width/8+(janela.width/8)*6, janela.height-janela.height/10,size=12,color=(255,255,255))



    # RENDER #
    musicaup.draw()
    baby.draw()
    medium.draw()
    hard.draw()
    janela.update()
