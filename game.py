from PPlay.window import *
from PPlay.sound import *
from PPlay.gameimage import *
from PPlay.sprite import *
from engine import galaxy, dificuldade, gamestate, keyboard, janela as window, clickSound as bullet_sound
import random

player = Sprite('resources/img/player.png')
player.set_position(window.width/2-player.width/2,window.height-player.height)
pshoot = Sprite('resources/img/playershoot.png')
enemy = Sprite('resources/img/invader.png')
eshoot = Sprite('resources/img/enemyshoot.png')
player.shoot_tick = 0
player.direction = -1
enemy.shoot_tick = 0
pshoot.delay = dificuldade[0]/2
player.move_delay = 500 - (10*dificuldade[0])
enemy_speed = dificuldade[0] * 200
eshoot.delay = 1/dificuldade[0]
enemy.direction = 1
enemy_direction = 1

matrix_x = int(random.uniform(3,10))
matrix_y = int(random.uniform(1,10)) 
bullets = []
enemies = [[0 for x in range(10)] for x in range(10)]

def spawn (i,j,matriz):
 for x in range(i):
        for y in range(j):
            # Define a posição
            enemy.set_position(x * enemy.width, y * enemy.height)
            # Define a direção do movimento, no caso para baixo
            enemy.direction = 1  # 1 = para baixo
            # Define randomicamente o intervalo entre os disparos
            eshoot.delay = random.uniform(0,15)/dificuldade[0]
            # Zera a variável de controle de disparos
            enemy.shoot_tick = 0
            # Coloca o inimigo recém criado na matriz
            enemies[x][y] = enemy


def adjust_bullet(actor, bullet):
    """
    Recebe o atirador e a bala, e ajusta sua posição
    :param actor: Instancia do jogador ou inimigo
    :param bullet: Instancia do projétil
    """
 
    # Calcula posição X da bala, utilizando como referência o 
    # centro do ator e armazena em x_fire
    x_fire = actor.x + (actor.width / 2) - (bullet.width / 2)
 
    # Calcula posição Y do projétil, utilizando como referência
    # a direção de movimento e o tamanho do jogador, salvando
    # o resultado na variável y_fire
    if actor.direction == -1:
        y_fire = actor.y
    elif actor.direction == 1:
        y_fire = actor.y + actor.height - bullet.height
 
    # Transfere o valor das variáveis auxiliares x_fire e y_fire
    # para o projétil
    bullet.x = x_fire
    bullet.y = y_fire
     
    # Define direção do projétil
    bullet.direction = actor.direction



def shoot(shooter):
    """
    Cria um bullet, associando-o a um ator
    :param shooter: Ator responsável pelo disparo (jogador ou inimigo)
    """
 
    # Reproduz o som do disparo
    bullet_sound.play()
 
    # Zera o contador de último disparo
    shooter.shoot_tick = 0
 
    # Cria uma nova bullet, dependendo de quem for que atirou
    if shooter.direction == -1:
        b = pshoot
    elif shooter.direction == 1:
        b = eshoot
         
    # Ajusta a posição inicial e a direção do projétil
    adjust_bullet(shooter, b)
 
    # Adiciona o novo projétil que criamos para ser desenhado na tela
    bullets.append(b)


def update_counters():
    """
    Atualiza contadores do jogo
    """
 
    # Atualiza o contador de controle de tiro do jogador
    player.shoot_tick += window.delta_time()
 
    # Atualiza o contador de controle de tiro de cada inimigo
    # presente na matriz de inimigos
    for row in range(matrix_x):
        for column in range(matrix_y):
            if enemy != 0:
                enemy.shoot_tick += window.delta_time()

def skill_lvl():
    pshoot.delay = dificuldade[0]/2
    player.move_delay = 500 - (10*dificuldade[0])
    enemy_speed = dificuldade[0] * 200
    eshoot.delay = 1/dificuldade[0]

def player_shoot():
    """
    Ação de atirar
    """
    # Verifica se o jogador apertou o botão de disparar
    if keyboard.key_pressed("space"):
        # Verifica se já pode disparar
        if player.shoot_tick > pshoot.delay:
            # Chama a função shoot(), para que ela efetue do disparo
            shoot(player)

def player_movement():
    """
    Ação de movimento da nave do jogador
    """
 
    # Atualiza a posição do jogador
    player.move_key_x(player.move_delay * window.delta_time())

    # Não permite que a lateral esquerda da nave ultrapasse a
    # lateral esquerda da tela, onde x = 0
    if player.x <= 0:         player.x = 0     # Não permite que a lateral esquerda da nave ultrapasse a
    # lateral direita da tela, onde x = largura da tela.
    if player.x + player.width >= window.width:
        player.x = window.width - player.width

def bullet_movement():
    """
    Realiza a movimentação de cada bala em jogo
    """
 
    # Para cada bala instanciada no jogo
    for b in bullets:
 
        # Atualiza a sua posição, baseado em sua direção
        b.move_y(200 * b.direction * window.delta_time() * dificuldade[0])
 
        # Verifica se saiu da tela e, caso tenha saído, destrói o projétil
        if b.y < -b.height or b.y > window.height + b.height:
            bullets.remove(b)

def enemy_movement():
    """
    Realiza a movimentação de cada inimigo
    """
 
    # Acessando variáveis globais
    global enemy_direction
    # Cria variável de controle
    inverted = False
 
    # Calcula a nova posição da matriz de inimigos
    new_position = enemy_speed * enemy_direction * window.delta_time()
 
    # Percorre toda a matriz de inimigos
    for row in range(matrix_x):
        for column in range(matrix_y):
            # Caso a posição esteja preenchida, isto é, o inimigo
            # ainda esteja vivo, efetua as ações em seguida
            if enemy != 0:
                # Move o inimigo para sua nova posição
                enemy.move_x(new_position)
 
                # Caso já tenha alcançado o intervalo de disparo,
                # efetua um novo disparo
                if enemy.shoot_tick > eshoot.delay :
                    shoot(enemy)
                    enemy.shoot_tick = 0
 
                    # Gera um novo intervalo de disparo aleatório
                    eshoot.delay = random.uniform(0,15)/dificuldade[0]
 
                # Verifica se nenhuma extremidade da matriz bateu na parede
                if not inverted:
                    # Se bateu na parede, então inverte a direção da matriz
                    # Altera direção para direita
                    if enemy.x <= 0:
                        enemy_direction = 1
                        inverted = True
                        # Altera direção para esquerda
                    elif enemy.x + enemy.width >= window.width:
                        enemy_direction = -1
                        inverted = True

def check_enemy_collision(b):
   """
   Verifica se o projétil colidiu com o inimigo
   :param b: Instância de projétil
   """
 
   # Percorre toda a matriz de inimigos
   for row in range(matrix_x):
       for column in range(matrix_y):
           if enemy!= 0:
               # Se o inimigo ainda estiver vivo!= 0),
               # Caso tenha havido colisão, remove a bala e o
               # inimigo do jogo
               # verifica se o disparo b colidiu com o mesmo
               if b.collided(enemy):
                   bullets.remove(b)
                   enemy = 0
 
                   # Atualiza a pontuação do jogador
                   player.score += (50 * dificuldade[0])
 
                   # Interrompe a função, pois o projétil foi destruído
                   # e não poderá colidir com mais nenhum inimigo
                   return

def bullet_ship_collision():
   """
   Verifica se os disparos colidiram com alguma nave
   """
 
   # Acessando variável global
   global gamestate
 
   # Para cada instância dos disparos
   for b in bullets:
       # Se for disparo do jogador
       if b.direction == -1:
           # Verifica se bateu em algum inimigo
           check_enemy_collision(b)
 
       # Se for disparo do inimigo
       elif b.direction == 1:
           # Verifica se bateu no jogador
           if b.collided(player):
               # Se bateu no jogador, define o fim de jogo
               gamestate[0] = 0

def bullet_bullet_collision():
   """
   Verifica se o projétil colidiu com alguma outro projétil
   """
   
   # Para cada instância de projétil
   for b1 in bullets:
       # Se for projétil do jogador
       if b1.direction == -1:
           # Verifica em todas as instâncias se ele colidiu com outro
           for b2 in bullets:
               # Verifica se o projétil atual é inimigo
               if b2.direction == 1:
                   # Se for inimigo, verifica se existiu colisão
                   if b1.collided(b2):
                       # Se houver colisão, remove os dois projéteis 
                       bullets.remove(b1)
                       bullets.remove(b2)
                         
                       break

def render():
    """
    Desenha todos os elementos na tela
    """
    print ("render")
    galaxy.draw()
    # Desenha todas as instâncias de projétil
    for b in bullets:
        b.draw()
 
    # Percorre todo a matriz de inimigos
    for row in range(matrix_x):
        for column in range(matrix_y):
            # Se o inimigo estiver vivo (!=0), desenha o inimigo
            if enemy!= 0:
                enemy.draw()
 
    # Desenha a nave do jogador
    player.draw()

def win():
    """
    Função para verificar se o jogador ganhou
    Caso afirmativo, reinicia o jogo
    """
 
    # Criamos o acesso às variáveis globais
    global matrix_x
    global matrix_y
    global dificuldade
    global gamestate
 
    # Criamos uma variável de controle, para sabermos se o jogador ganhou o jogo
    won = True
 
    # Verifica em todas as linhas se ainda existe algum inimigo vivo
    for row in range(matrix_x):
        if won:
            for column in range(matrix_y):
                if enemy != 0:
                    # Se ele encontrar algum inimigo vivo, seta a variável
                    # won como False e quebra a cadeia de repetições
                    won = False
                    break
 
    if won:
        # Se o jogo percorrer toda a matriz e não encontrar 
        # nenhum inimigo vivo, reinicia o jogo
        dificuldade[0] += 1
        gamestate[0] = 0

def gameplay():
    win()
 
    # Atualiza os contadores
    update_counters()

    # Atualiza a movimentação do jogador
    player_movement()

    # Controle os tiros a cada intervalo
    player_shoot()

    # Atualiza o movimento dos disparos
    bullet_movement()

    # Atualiza o movimento dos inimigos
    enemy_movement()

    # Verifica a colisão de projéteis contra naves
    bullet_ship_collision()

    # Verifica colisões entre projéteis
    bullet_bullet_collision()

    ## Renderiza todos os dados na tela ##
    render()

    window.update()
