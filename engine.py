#!/usr/bin/python3

from PPlay.sound import *
from PPlay.window import *
from PPlay.gameimage import *
import random
import sys
from settings import *

## Debug
#log = open('log.txt','w')

## Janela
janela = Window(ww,wh)
janela.set_title('Invaders')
janela.set_background_color([0,0,0])

## Gamestate
gamestate  = [0]

## Menu
jojo0 = GameImage('resources/img/rumble.png')
jojo1 = GameImage('resources/img/rumble.png')
jojo0.x = 0
jojo0.y = random.randint(0,janela.height)
jojo1.x = -jojo1.width
jojo1.y = random.randint(0,janela.height)
background_roll_speed = 300
fundo = ('resources/img/jojo.jpg')
galaxy = GameImage('resources/img/galaxy.jpg')

from menu import *
from hiscore import *
from options import *

## Input
keyboard = Window.get_keyboard()
mouse = Window.get_mouse()
mouse_position = mouse.get_position()
mouse_x = mouse_position[0]
mouse_y = mouse_position[1]

## Sound

invaderSong = Sound('resources/sound/invaders.ogg')
invaderSong.set_volume(music_volume)
invaderSong.set_repeat(True)

clickSound = Sound('resources/sound/click.ogg')
clickSound.set_volume(effect_volume)

## Menu Items
start = GameImage('resources/img/start.png')
start.set_position(janela.width/2-start.width/2,janela.height/6-start.height/2)

options = GameImage('resources/img/options.png')
options.set_position(janela.width/2-options.width/2,start.y+start.height*1.5)

score = GameImage('resources/img/scores.png')
score.set_position(janela.width/2-score.width/2,options.y+start.height*1.5)

ebutton = GameImage('resources/img/exit.png')
ebutton.set_position(janela.width/2-ebutton.width/2,score.y+start.height*1.5)

from game import gameplay, skill_lvl

## Main
def main():


    while True:

        if gamestate[0] == 0:
            menu(janela,mouse,invaderSong,clickSound,keyboard,jojo0,jojo1,background_roll_speed,start,options,ebutton,galaxy,score)
            # log.write ('global gamestate='+str(gamestate))

        elif gamestate[0] == 1:
            if invaderSong.is_playing():
                invaderSong.stop()
            gameplay()

        elif gamestate[0] == 2:
            options_menu(keyboard, clickSound, mouse, janela, galaxy,invaderSong)

        elif gamestate[0] == 3:
            score_screen(galaxy, janela, mouse, clickSound, keyboard)

        if len(sys.argv)>1:
            if sys.argv[1] == 'd' or sys.argv[2] == 'd':
                janela.draw_text(str(janela.delta_time()),janela.width*0.90,janela.height-janela.height*0.95,size = 12, color = (255,255,255))


while __name__ == '__main__':
    main()
